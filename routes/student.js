var express = require("express");
var router = express.Router();
var mongoose = require('mongoose');

//schema
var studentschema = mongoose.Schema({
  name:{
    type: String,
    required: true
  },
  age:{
    type: String,
    required: true
  },
  department:{
    type: String,
    required: true
  },
  city:{
    type: String,
    required: true
  }
});

router.get('/',function(req, res){
  res.send('please use /students');
});

router.get('/students/user', function(req, res, next) {
  res.render('student');
});

router.get('/students',function(req, res){
   Student.getStudent(function(err,students){
     if(err){
       throw err;
     }
     res.json(students);
   });
});

router.post('/api/students',function(req,res){
  var stud = req.body;
  Student.addStudent(stud,function(err,stud){
    if(err){
      throw err;
    }
    res.json(stud);
  });
});

var Student = module.exports = mongoose.model('Student',studentschema);

//get Student
module.exports.getStudent = function(callback,limit){
Student.find(callback).limit(limit);
}

module.exports.addStudent = function(stud,callback){
Student.create(stud,callback);
}

module.exports = router;
