# README #

This is a simple project retrieve and store data in a mongodb. Trigger endpoints using postman.

## **Initial process** ##

### Step 1: Clone Repository ###

      Git clone <bitbucket link>

### Step 2: Install dependencies ###
        
      npm install
      
### Step 3: Run project ###

      npm start
 
### Execution endpoints (postman): ###

1.GET  http://localhost:3000/students  -----> to get all users

2.POST http://localhost:3000/api/students  -----> to post details